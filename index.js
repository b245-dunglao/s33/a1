console.log("S33 Activity API");

// fetch("https://jsonplaceholder.typicode.com/todos", {method: "GET"})
// 	.then(response => response.json())
// 	.then(result => console.log(result.map(element => element.title)));

fetch("https://jsonplaceholder.typicode.com/todos", {method: "GET"})
	.then(response => {
			return response.json()
		})
	.then(result => {
		let mapTitle = result.map(element => element.title)
		console.log(mapTitle);
		});


fetch("https://jsonplaceholder.typicode.com/todos/21", {method: "GET"})
	.then(response => {
			return response.json()
		})
	.then(result => {
		console.log(`Title:  ${result.title}`);
		console.log(`Completed: ${result.completed}`);
		});



	fetch("https://jsonplaceholder.typicode.com/todos", {

		method: "POST", 
		headers: {
			"Content-Type":"application/json"
		},
		body: JSON.stringify({
			title: "Code Activity s33 a1",
			completed: false,
			userId: 21
		})

	})	
	.then(response => response.json())
	.then(result => console.log(result));



	fetch("https://jsonplaceholder.typicode.com/todos/21", {
		method: "PUT", 
		headers: {
			"Content-Type":"application/json"
		},
		body: JSON.stringify({
			title: "Update Title for activity",
			completed: true,
		})
	})	
	.then(response => response.json())
	.then(result => console.log(result));


	fetch("https://jsonplaceholder.typicode.com/todos", {

		method: "POST", 
		headers: {
			"Content-Type":"application/json"
		},
		body: JSON.stringify({
			title: "Updated To do list item",
			description: "To update the my to do list with a different data structure",
			status: "pending",
			dateCompleted: "pending",
			userId: "21"
		})

	})	
	.then(response => response.json())
	.then(result => console.log(result));



	fetch("https://jsonplaceholder.typicode.com/todos/21", {
		method: "PATCH",
		headers: {
			'Content-Type':"application/json"
		},
		body: JSON.stringify({
			completed: true,
			dateCompleted: "01/31/2023"


		})
	})
	.then(response => response.json())
	.then(result => console.log(result));


	// Delete by archive

	fetch("https://jsonplaceholder.typicode.com/todos/21", {
		method: "PATCH",
		headers: {
			'Content-Type':"application/json"
		},
		body: JSON.stringify({
			isActive: false


		})
	})
	.then(response => response.json())
	.then(result => console.log(result));

	// Delete by method
	
	fetch("https://jsonplaceholder.typicode.com/todos/21", {
		method: "DELETE"})
	.then(response => response.json())
	.then(result => console.log(result));



